## A simple Container implementation

The idea is copied from [symfony](https://github.com/symfony/symfony/tree/master/src/Symfony/Component/DependencyInjection).
It's a simplification for the above mentioned implementation, the services need to be added manually to the class.

### A usage example
```
<?php

namespace Container;

final class SampleContainer extends AbstractContainer
{
    protected $services = [
        'repository.user' => null,
        'manager.redis_cache' => null,
    ];

    protected function getRepository_UserService(
        string $id, bool $newInstance
    ): \SampleProject\Repository\UserRepository {
        if ($newInstance || ! $this->services[$id]) {
            $this->services[$id] = new \SampleProject\Repository\UserRepository(
                $this->getService('manager.redis_cache', [], $newInstance)
            );
        }

        return $this->services[$id];
    }

    protected function getManager_RedisCacheService(
        string $id, bool $newInstance
    ): \SampleProject\Manager\RedisCacheManager {
        if ($newInstance || ! $this->services[$id]) {
            $this->services[$id] = new \SampleProject\Manager\RedisCacheManager();
        }

        return $this->services[$id];
    }
}
```
