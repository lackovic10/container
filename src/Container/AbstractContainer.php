<?php

namespace Container;

use Psr\Container\ContainerInterface;
use Exception;

abstract class AbstractContainer implements ContainerInterface
{
    /**
     * {@inheritdoc}
     */
    public function getService(string $id, array $arguments = []): object
    {
        return $this->getServiceBase($id, $arguments, false);
    }

    public function getNewService(string $id, array $arguments = []): object
    {
        return $this->getServiceBase($id, $arguments, true);
    }

    private function getServiceBase(string $id, array $arguments = [], bool $newInstance = false): object
    {
        if (! array_key_exists($id, $this->services)) {
            throw new Exception(sprintf('Invalid id: %s', $id));
        }

        $methodName = $this->getServiceMethodName($id);

        return call_user_func_array(
            'static::'.$methodName,
            array_merge([$id], [$newInstance], $arguments)
        );
    }

    protected function getServiceMethodName(string $id): string
    {
        $methodName = join(
            '_',
            array_map(
                function ($idSegment) {
                    return join(
                        '',
                        array_map('ucfirst', explode('_', $idSegment))
                    );
                },
                explode('.', $id)
            )
        );

        return 'get'.$methodName.'Service';
    }

    public function has($id)
    {
        return array_key_exists($id, $this->services);
    }

    public function get($id)
    {
        return $this->getNewService($id, []);
    }
}
