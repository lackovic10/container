<?php

namespace Container;

final class SampleContainer extends AbstractContainer
{
    protected $services = [
        'sample_service' => null,
    ];

    protected function getSampleServiceService(
        string $id, bool $newInstance
    ): \Container\SampleService {
        if ($newInstance || ! $this->services[$id]) {
            $this->services[$id] = new \Container\SampleService();
        }

        return $this->services[$id];
    }
}

