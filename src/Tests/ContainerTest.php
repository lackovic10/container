<?php

namespace Container;

use PHPUnit\Framework\TestCase;
use Container\SampleContainer;

final class ContainerTest extends TestCase
{
    private $container;

    public function setUp(): void
    {
        parent::setUp();

        $this->container = new SampleContainer();
    }

    public function testHas(): void
    {
        $this->assertTrue($this->container->has('sample_service'));
        $this->assertFalse($this->container->has('non_existing_service'));
    }

    public function testGetService(): void
    {
        $this->assertInstanceOf('Container\SampleService', $this->container->getService('sample_service'));
    }

    public function testGet(): void
    {
        $this->assertInstanceOf('Container\SampleService', $this->container->get('sample_service'));
    }
}
